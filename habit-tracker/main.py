import requests
from datetime import datetime

TOKEN = 'jhlkjmjhlfsdhqml'
USERNAME = 'twandev1336'
PIXELA_URL = 'https://pixe.la/v1/users'
GRAPH_PATH = f"/{USERNAME}/graphs"
GRAPH_ID = 'testgraph1'

user_data = {
    'token': TOKEN,
    'username': USERNAME,
    'agreeTermsOfService': 'yes',
    'notMinor': 'yes',
}
#   #   #       CREATE USER         #   #   #
# response = requests.post(url=PIXELA_URL, json=user_data)
# response.raise_for_status()
# print(response.text)

headers = {
    'X-USER-TOKEN': TOKEN,
}
#   #   #       CREATE GRAPH        #   #   #
# graph_data = {
#     'id': GRAPH_ID,
#     'name': 'test_graph',
#     'unit': 'Km',
#     'type': 'float',
#     'color': 'ajisai'
#     # shibafu (green), momiji (red), sora (blue), ichou (yellow), ajisai (purple) and kuro (black)
# }
# response = requests.post(f'{PIXELA_URL}{GRAPH_PATH}', json=graph_data, headers=headers)
# response.raise_for_status()
# print(response.headers)

#   #   #       POST A PIXEL        #   #   #
today_formatted = datetime.now().strftime("%Y%m%d")
tmp = f'{PIXELA_URL}{GRAPH_PATH}/{GRAPH_ID}'
pixel_data = {
    'date': today_formatted,
    'quantity': '100.2',
}
response = requests.post(url=f'{PIXELA_URL}{GRAPH_PATH}/{GRAPH_ID}', json=pixel_data, headers=headers)
response.raise_for_status()
print('posting a pixel:', response.text)


#   #   #       DELETE A PIXEL      #   #   #
day_to_delete_formatted = datetime(year=2021, month=12, day=27).strftime("%Y%m%d")
test_data = {
    'date': day_to_delete_formatted,
    'quantity': '1000.2',
}
requests.post(
    url=f'{PIXELA_URL}{GRAPH_PATH}/{GRAPH_ID}',
    json=test_data,
    headers=headers,
)

delete_response = requests.delete(url=f'{PIXELA_URL}{GRAPH_PATH}/{GRAPH_ID}/{day_to_delete_formatted}', headers=headers)
delete_response.raise_for_status()
print('deleting pixel:', delete_response.text)

#   #   #       UPDATE A PIXEL        #   #   #
new_data = {
    'date': today_formatted,
    'quantity': '10',
}
response = requests.put(url=f'{PIXELA_URL}{GRAPH_PATH}/{GRAPH_ID}/{today_formatted}', headers=headers, json=new_data)
print('updating pixel:', response.text)

