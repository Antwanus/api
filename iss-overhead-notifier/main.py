from pprint import pprint
import requests
import datetime as dt
import smtplib

response = requests.get(url="http://api.open-notify.org/iss-now.json")
response.raise_for_status()
response_body = response.json()

iss_lat = float(response_body["iss_position"]["latitude"])
iss_long = float(response_body["iss_position"]["longitude"])

my_lat = 51.219448
my_long = 4.402464

# lat/long of Antwerp
parameters = {
    "lat": my_lat,
    "long": my_long,
    "formatted": 0,
}
response = requests.get("https://api.sunrise-sunset.org/json", params=parameters)
response.raise_for_status()
response_body = response.json()

sunrise_hour = int(response_body["results"]["sunrise"].split("T")[1].split("+")[0].split(":")[0])
sunset_hour = int(response_body["results"]["sunset"].split("T")[1].split("+")[0].split(":")[0])
current_datetime = dt.datetime.now()


def send_email():
    with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
        connection.starttls()
        connection.login(user="antwanus1337@gmail.com", password="geheimpje")
        connection.sendmail(
            from_addr="antwanus1337@gmail.com",
            to_addrs="antwanus1337@gmail.com",
            msg=f"Subject:The ISS is visible!\n\nLOOK UP!"
        )


# Check if it's dark
if current_datetime.hour not in range(sunrise_hour, sunset_hour + 1):
    print("it is dark")
    # Check if ISS is in range (+/-5) of my_lat and my_long
    print(f"(my_lat, my_long):\t\t({my_lat}, {my_long})\n(iss_lat, iss_long):\t({iss_lat}, {iss_long})")
    if my_lat in range(int(iss_lat - 5), int(iss_lat + 5) + 1) \
            and my_long in range(int(iss_long - 5), int(iss_long + 5) + 1):
        send_email()
