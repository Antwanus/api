import requests

response = requests.get("http://api.open-notify.org/iss-now.json")

# if response.status_code == 200:
#     print("Http status OK")
# elif response.status_code == 404:
#     raise Exception("That resource does not exist")
# elif response.status_code == 401:
#     raise Exception("You have no authorization for this resource")
# ...
response.raise_for_status()


print(type(response.json()))
print(response.json())
longitude = response.json()['iss_position']['longitude']
latitude = response.json()['iss_position']['latitude']
print(f'Longitude: {longitude}\tLatitude: {latitude}')

